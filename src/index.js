// SET UP CONFIG
var Web = require("web3");
var GanacheUrl = "https://mainnet.infura.io/v3/a5509144fe434cb6b4001d4b4af2243b";
var InfuraWS = "wss://rinkeby.infura.io/ws/v3/a5509144fe434cb6b4001d4b4af2243b"
//var GanacheUrl = "HTTP://127.0.0.1:7545";
var web3 = new Web(InfuraWS);
var EthereumTransaction = require("ethereumjs-tx").Transaction;

// SET UP ADDRESSES
var receiver = "0xcD4b008789742eb41c0160c07384D8d03fA3A7B4";

function ascii2hex(num){
  return "0x" + num.toString(16);
}

//SET UP TRANSACTION DATA
// var rawTransaction = {
//   nonce: 0,
//   gasLimit: 2000000,
//   gasPrice: 30000,
//   value: 1100000000000000000,
//   to: receiver,
//   data: "0x",
// }
var privateKey = "b20caabf34bce1533e3a76abefbbe0f2bdbb803d62355ac085c9a662ec784eb4"
//var privateKey = 'd4242dcdf0a9e32324316ec7d5cd969fcdf087eea6d8325f0a602fb972796bf3';
var privateKeyAsHex = Buffer.from(privateKey, 'hex');
try {
    let rawTransaction;
    const buildRawTransaction = async () => {
      rawTransaction = {
        nonce: await web3.eth.getTransactionCount('0xA95B5bdF5DD35B243457cc40c152a8D7fCA23006', 'pending'),
        gasLimit: 220000,
        gasPrice: 40000,
        value: 1100000000000000000,
        to: receiver,
        data: "0x",
      }
    } 
  buildRawTransaction();  
  var ethtransaction = new EthereumTransaction(rawTransaction, {chain: 'rinkeby'});
  ethtransaction.sign(privateKeyAsHex);
} catch (err) {
  console.log("ERROR: ", err)
}
// Serialize the transaction and send it
var serializedTransaction = ethtransaction.serialize();
console.log("SERIALIZED TRANSACTION: ", serializedTransaction.toString('hex'))
const sendTransaction = async () => {
  return await web3.eth.sendSignedTransaction('0x' + serializedTransaction.toString('hex')).catch(err => console.log(`SHIT, this happened: ${err}`));
}
sendTransaction().then(res => console.log(res));
//web3.eth.getGasPrice().then(res => console.log(res))
//web3.eth.getUncle(12270850, 1).then(res => console.log(res))
//web3.eth.getBlockTransactionCount(12270850).then(res => console.log(res))


